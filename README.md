Q JavaScript Framework
====

Because we don't need to use jQuery for everything!<br /> <br />

Do not use this framework for IE<9.

####Examples:
```javascript
Q('selector').fadeOut();
Q('selector').toggle();
Q('selector').clone().hide();
// Methods are chainable

// More examples coming
```
