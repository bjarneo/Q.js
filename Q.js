/**
 * Q JavaScript Framework
 *
 * @author: Bjarne Øverli
 * @website: http://www.codephun.com
 * @twitter: @bjarneo_
 */
(function () {
    "use strict";

    var Q = function (params) {
        return new Query(params);
    };

    var Query = function (params) {

        var selector = document.querySelectorAll(params),
            i = 0;
        this.length = selector.length;

        for (; i < this.length; i++) {
            this[i] = selector[i];
        }

        return this;
    };

    Q.fn = Query.prototype =
    {
        init: function () {
            return this;
        },

        toggled: 0,

        /**
         * Add event listener
         * @param listener
         * @param func
         * @returns {*}
         */
        bind: function (listener, func) {
            var len = this.length;

            while(len--) {
                if (this[len].addEventListener) {
                    this[len].addEventListener(listener, func, false);
                } else {
                    this[len].attachEvent('on' + listener, func);
                }
            }
            return this;
        },

        /**
         * Clear element and add new html
         * @param html
         * @returns {*}
         */
        html: function(html) {
            if(html === undefined) {
                html = '';
            }
            this[0].innerHTML = html;

            return this;
        },


        /**
         * Append html
         * @param html
         * @returns {*}
         */
        appendHTML: function(html) {
            this[0].innerHTML += html;

            return this;
        },

        /**
         * Append Child
         * @param String/Array text
         * @param String node
         */
        append: function (text, node) {
            var newNode, i;

            if(text instanceof Array) {
                i = text.length;
                while (i--) {
                    newNode = document.createElement(node);
                    newNode.innerHTML = text[i];
                    this[0].appendChild(newNode);
                }
            } else {
                newNode = document.createElement(node);
                newNode.innerHTML = text;
                this[0].appendChild(newNode);
            }

            return this;
        },

        /**
         * Clone node + children
         * @returns {*}
         */
        clone: function() {
            var clone = this[0].cloneNode(true);
            return clone.outerHTML;
        },

        /**
         * Remove element and children.
         * IE < 9 won't work (lastChild/firstChild)
         * @param elem
         */
        remove: function () {
            var len = this.length;
            while (len--) {
                while (this[len].hasChildNodes()) {
                    this[len].removeChild(this[len].lastChild);
                }
                this[len].parentNode.removeChild(this[len]);
            }

            return this;
        },

        /**
         * Hide element(s) from DOM
         * @returns {*}
         */
        hide: function () {
            var len = this.length;
            while (len--) {
                this[len].style.display = 'none';
            }
            return this;
        },

        /**
         * Show element(s) from DOM
         * @returns {*}
         */
        show: function () {
            var len = this.length;
            while (len--) {
                this[len].style.display = 'block';
            }

            return this;
        },

        /**
         * Toggle element.
         * @returns {*}
         */
        toggle: function(speed) {

            switch (speed) {
                case 'slow':
                    speed = 50;
                    break;
                case 'medium':
                    speed = 25;
                    break;
                case 'fast':
                    speed = 10;
                    break;
                default:
                    speed = 20;
                    break;
            }


            if(this.toggled === 0 && this.css('display') !== 'none') {
                this.toggled = 1;
                this.fadeOut(speed);
            } else {
                this.toggled = 0;
                this.fadeIn(speed);
            }

            return this;
        },

        /**
         * Fade in elements
         * @param timer (ms)
         * @returns {*}
         */
        fadeIn: function (timer) {
            timer = (timer) ? timer : 50;
            var opacity = 0, self = this,
                interval = setInterval(function () {
                    var len = self.length;
                    self.show();
                    if (opacity >= 1) {
                        clearInterval(interval);
                        return;
                    }
                    while(len--) {
                        self[len].style.opacity = opacity.toFixed(1);
                    }
                    opacity += 0.1;
                }, timer);

            return this;
        },

        /**
         * Fade out elements
         * @param timer
         * @returns {*}
         */
        fadeOut: function (timer) {
            timer = (timer) ? timer : 50;
            var opacity = 1, self = this,
                interval = setInterval(function () {
                    var len = self.length;
                    if (opacity <= 0) {
                        clearInterval(interval);
                        self.hide();
                        while(len--) {
                            self[len].style.opacity = 1;
                        }
                        return;
                    }
                    while(len--) {
                        self[len].style.opacity = opacity.toFixed(1);
                    }
                    opacity -= 0.1;
                }, timer);

            return this;
        },

        /**
         * Set css property and value. Ex: 'background', 'red'
         * Get css property (array or string)
         * @param String property
         * @param String value
         * @support Doesn't support < IE9 (getComputedStyle)
         */
        css: function(property, value) {
            if(!value) {
                var i = property.length,
                    list = [];
                if(property instanceof Array) {
                    while(i--) {
                        list[i] = document.defaultView.getComputedStyle(this[0], null).getPropertyValue(property[i]);
                    }
                    return list;
                } else {
                    return document.defaultView.getComputedStyle(this[0], null).getPropertyValue(property);
                }
            } else {
                var len = this.length;
                while(len--) {
                    this[len].style[property] = value;
                }
            }
            return this;
        },

        /**
         * Object to array
         * @returns []
         */
        toArray: function () {
            var a = [], i = 0;
            try {
                return [].slice.call(this);
            } catch (e) {
                // ie < 9
                for (; i < this.length; i++) {
                    a.push(this[i]);
                }
                return a;
            }
        }
    };


    /**
     * Helper methods
     * @type {{ajax: Function, log: Function}}
     */
    var h = {

        /**
         * Ajax
         * @param params (object) data,type,url,dataType,success
         * todo: TONS (error, timeout ++)
         */
        ajax: function (params) {
            var request = new XMLHttpRequest(),
                type;

            if (params.data) {
                // loop through data properties and set to url.
                //request.open(params.type, params.url))
            } else {
                request.open(params.type, params.url);
            }

            request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status === 200) {
                    if (params.dataType) {
                        type = request.getResponseHeader('Content-type', 'application/' + params.dataType + ';charset=UTF-8');
                    } else {
                        type = request.getResponseHeader('Content-type');
                    }
                    if (typeof params.success === 'function') {
                        if (type.indexOf('xml') !== -1 && request.responseXML) {
                            params.success(request.responseXML);
                        } else if (type === 'application/json') {
                            params.success(JSON.parse(request.responseText));
                        } else {
                            params.success(request.responseText);
                        }

                    }
                }
            };

            request.send(null);
        },

        /**
         * Use as logger (if console exists)
         * @param text
         */
        log: function (text) {
            if (typeof console == 'object') {
                console.log(text);
            }
        }
    };

    if (!window.Q || !window.h) {
        window.Q = Q;
        window.h = h;
    }
})();