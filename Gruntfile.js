module.exports = function(grunt){
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            all: ['Q.js']
        },
        uglify: {
            build: {
                files: {
                    'q.min.js': ['Q.js']
                }
            }
        },
        watch: {
            js: {
                files: ['Q.js'],
                tasks: ['uglify', 'jshint']
            }
        }

    });

    grunt.registerTask('default', []);

};